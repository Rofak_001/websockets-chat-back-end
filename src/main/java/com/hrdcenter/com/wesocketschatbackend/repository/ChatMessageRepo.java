package com.hrdcenter.com.wesocketschatbackend.repository;

import com.hrdcenter.com.wesocketschatbackend.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatMessageRepo extends JpaRepository<ChatMessage, Integer> {
}
