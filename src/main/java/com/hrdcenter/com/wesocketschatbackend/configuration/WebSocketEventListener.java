package com.hrdcenter.com.wesocketschatbackend.configuration;

import com.hrdcenter.com.wesocketschatbackend.model.ChatMessage;
import com.hrdcenter.com.wesocketschatbackend.model.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
@Component
public class WebSocketEventListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketEventListener.class);

    private SimpMessageSendingOperations simpMessageSendingOperations;
    @Autowired
    public void setSimpMessageSendingOperations(SimpMessageSendingOperations simpMessageSendingOperations) {
        this.simpMessageSendingOperations = simpMessageSendingOperations;
    }
    @EventListener
    public void handleWebSocketConnectListener(final SessionConnectedEvent event){
        LOGGER.info("Bing bang. We have a new Cheeky little connection!");
    }
    @EventListener
    public void handleWebSocketDisconnctListener(final SessionDisconnectEvent event){
        final StompHeaderAccessor headerAccessor=StompHeaderAccessor.wrap(event.getMessage());
        final String username=(String) headerAccessor.getSessionAttributes().get("username");
        final ChatMessage chatMessage=new ChatMessage();
        chatMessage.setType(MessageType.DISCONNECT);
        chatMessage.setSender(username);
        simpMessageSendingOperations.convertAndSend("/topic/messages/"+headerAccessor.getSessionAttributes().get("username"),chatMessage);
    }
}
