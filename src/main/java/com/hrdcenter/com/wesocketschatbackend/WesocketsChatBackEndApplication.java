package com.hrdcenter.com.wesocketschatbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WesocketsChatBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(WesocketsChatBackEndApplication.class, args);
    }

}
