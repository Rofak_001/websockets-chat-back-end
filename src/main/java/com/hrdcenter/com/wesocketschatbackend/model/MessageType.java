package com.hrdcenter.com.wesocketschatbackend.model;

public enum MessageType {
    CHAT,
    CONNECT,
    DISCONNECT,
}
