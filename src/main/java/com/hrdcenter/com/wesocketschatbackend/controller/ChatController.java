package com.hrdcenter.com.wesocketschatbackend.controller;

import com.hrdcenter.com.wesocketschatbackend.model.ChatMessage;
import com.hrdcenter.com.wesocketschatbackend.model.User;
import com.hrdcenter.com.wesocketschatbackend.repository.ChatMessageRepo;
import com.hrdcenter.com.wesocketschatbackend.repository.UserRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChatController {
    private SimpMessagingTemplate simpMessagingTemplate;
    private ChatMessageRepo chatMessageRepo;
    private UserRep userRep;

    @Autowired
    public void setUserRep(UserRep userRep) {
        this.userRep = userRep;
    }

    @Autowired
    public void setChatMessageRepo(ChatMessageRepo chatMessageRepo) {
        this.chatMessageRepo = chatMessageRepo;
    }

    @Autowired
    public void setSimpMessagingTemplate(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/chat/{to}")
    public void sendMessage(@DestinationVariable String to, ChatMessage message) {
        System.out.println("handling send message: " + message + " to: " + to);
        simpMessagingTemplate.convertAndSend("/topic/messages/" + to, message);
    }

    @PostMapping("/register")
    @CrossOrigin
    public ResponseEntity<String> register(@RequestBody User user) {
        userRep.save(user);
        return ResponseEntity.ok("Ok");
    }

    @GetMapping("/getAll")
    @CrossOrigin
    public List<User> getAllUser() {
        return userRep.findAll();
    }
}
